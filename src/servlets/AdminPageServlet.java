package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import models.User;
import nosqldb.NoSqlDataBase;

public class AdminPageServlet extends javax.servlet.http.HttpServlet implements javax.servlet.Servlet {

	private static final long serialVersionUID = 1L;
	private NoSqlDataBase database = NoSqlDataBase.getInstance();

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		performTask(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		performTask(request, response);
	}

	private void performTask(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		ArrayList<User> users = database.getUsers();
		out.println("<table>");
		for (User user : users) {
			// if (!user.getUsername().equals("admin"))
			out.println("<tr>");
			out.println("<td>Username :" + user.getUsername() + "</td><td>");
			if (user.isPremium())
				out.println("PREMIUM");
			else
				out.println("NO-PREMIUM");
			out.println("</td></tr>");
		}
		out.println("</table>");

		out.println("<h2>Give user premium</h2>" + "<form action='changePremiumUserServlet' method='post'>"
				+ "Username: <input type='text' name='usertochangepremium' /> <br />"
				+ "<input type='hidden' name='premium' value='true' />" + "<input type='submit' value='Submit' />"
				+ "</form>");

		out.println("<h2>De-premium user</h2>" + "<form action='changePremiumUserServlet' method='post'>"
				+ "Username: <input type='text' name='usertochangepremium' /> <br />"
				+ "<input type='hidden' name='premium' value='false' />" + "<input type='submit' value='Submit' />"
				+ "</form>");

	}

}
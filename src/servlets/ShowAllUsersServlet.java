package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import models.User;
import nosqldb.NoSqlDataBase;

public class ShowAllUsersServlet extends javax.servlet.http.HttpServlet implements javax.servlet.Servlet {

	private static final long serialVersionUID = 1L;
	private NoSqlDataBase database = NoSqlDataBase.getInstance();

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		performTask(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		performTask(request, response);
	}

	private void performTask(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		ArrayList<User> users = database.getUsers();

		for (User user : users) {
			//if (!user.getUsername().equals("admin"))
			out.println("---------------------------");
			out.println("Username :" + user.getUsername());
			out.println("Password :" + user.getPassword());
			out.println("Email :" + user.getEmail());
		}
	}

}
package servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import models.AdressElement;
import models.User;

public class ShowProfileServlet extends javax.servlet.http.HttpServlet implements javax.servlet.Servlet {

	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		performTask(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		performTask(request, response);
	}

	private void performTask(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		User user = (User) request.getSession().getAttribute("User");
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("Twoje Dane : <br />");
		out.println("Username :" + user.getUsername());
		out.println("<br/>Password :" + user.getPassword());
		out.println("<br/>Email :" + user.getEmail());

		out.println("<br/>Twoje adresy :");
		for (AdressElement tmpAdress : user.getAdressy()) {
			out.println("<br/>" + tmpAdress.getAdressName());
			out.println("<br/>Wojewodztwo :" + tmpAdress.getWojewodztwo());
			out.println("<br/>Miasto :" + tmpAdress.getMiasto());
			out.println("<br/>Kod pocztowy :" + tmpAdress.getKodPocztowy());
			out.println("<br/>Ulica :" + tmpAdress.getUlica());
			out.println("<br/>Numer domu/mieszkania :" + tmpAdress.getNumerDomu());
			out.println("<form action='changeAdressServlet' method='post'>"
					+ "<input type='hidden' name='adresstype' value='" + tmpAdress.getAdressName() + "' />"
					+ "<input type='submit' value='Zmien adres zameldowania' />" + "</form>");
		}
		out.println("<form action='AddAdressServlet' method='post'>"
				+ "<input type='submit' value='Dodaj nowy adres' />" + "</form>");
		out.println("<form action='RemoveAdressServlet' method='post'>"
				+ "<input type='submit' value='Usun adres' />" + "</form>");

		out.println("<br/><a href = 'showUsers'>Pokaz wszystkich uzytkownikow <a/>");

		if (user.getUsername().equals("admin")) {
			out.println("<br/><a href = 'changePremiumUserServlet'> AdminPage <a/>");
		}
	}

}
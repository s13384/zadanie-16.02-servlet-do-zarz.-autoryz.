package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import models.AdressElement;
import models.User;
import nosqldb.NoSqlDataBase;

public class ChangeAdressServlet extends javax.servlet.http.HttpServlet implements javax.servlet.Servlet {

	private static final long serialVersionUID = 1L;
	private NoSqlDataBase database = NoSqlDataBase.getInstance();

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		performTask(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		performTask(request, response);
	}

	private void performTask(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		String adresstype = (String) request.getAttribute("adresstype");
		User user = (User) request.getSession().getAttribute("User");
		AdressElement tmpAdress = user.getAdres(adresstype);

		out.println(
				"<form action='changeAdressServlet2' method='post'>" 
					+ "<input type='hidden' name='adresstype' value='" + tmpAdress.getAdressName() + "' />" 
					+ "<input type='text' name='wojewodztwo' value='" + tmpAdress.getWojewodztwo() + "'>"
					+ "<input type='text' name='miasto' value='" + tmpAdress.getMiasto() + "'>"
					+ "<input type='text' name='kodpocz' value='" + tmpAdress.getKodPocztowy() + "'>"
					+ "<input type='text' name='ulica' value='" + tmpAdress.getUlica() + "'>"
					+ "<input type='text' name='nrdom' value='" + tmpAdress.getNumerDomu() + "'>"
					+ "<input type='submit' value='Zatwierdz' />" + "</form>");

	}

}
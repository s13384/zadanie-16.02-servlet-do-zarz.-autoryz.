package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import models.User;
import nosqldb.NoSqlDataBase;

public class ChangePremiumUserServlet extends javax.servlet.http.HttpServlet implements javax.servlet.Servlet {

	private static final long serialVersionUID = 1L;
	private NoSqlDataBase database = NoSqlDataBase.getInstance();

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		performTask(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		performTask(request, response);
	}

	private void performTask(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		boolean premium;

		if (request.getAttribute("premium").equals("true")) {
			premium = true;
		} else {
			premium = false;
		}

		database.getUser((String) request.getAttribute("usertochangepremium")).setPremium(premium);
		out.println("<a href = 'adminPageServlet'> Wiecej zmian <a/><br/>");
		out.println("<a href = 'showProfile'> Profil <a/>");

	}

}
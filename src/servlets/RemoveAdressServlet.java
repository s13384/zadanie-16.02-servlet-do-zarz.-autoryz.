package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import models.AdressElement;
import models.User;
import nosqldb.NoSqlDataBase;

public class RemoveAdressServlet extends javax.servlet.http.HttpServlet implements javax.servlet.Servlet {

	private static final long serialVersionUID = 1L;
	private NoSqlDataBase database = NoSqlDataBase.getInstance();

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		performTask(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		performTask(request, response);
	}

	private void performTask(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		String adresstype = (String) request.getAttribute("adresstype");
		User user = (User) request.getSession().getAttribute("User");
		ArrayList<AdressElement> userAdreses = user.getAdressy();

		int i;
		out.print("<table>");
		for (i = 3; i < user.getAdressy().size(); i++) {
			out.print("<tr>");
			out.println("<td>" + userAdreses.get(i) + "</td>");
			out.print("<td>");
			out.println("<form action='AddAdressServlet2' method='post'>"
					+ "<input type='hidden' name='adressremovename' value='" + userAdreses.get(i) + "' />"
					+ "<input type='submit' value='Usun adres' />" + "</form>");
			out.print("</td>");
			out.print("</tr>");
		}
		out.print("</table>");

	}

}
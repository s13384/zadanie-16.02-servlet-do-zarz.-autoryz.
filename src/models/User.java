package models;

import java.util.ArrayList;

public class User {

	private String username;
	private String password;
	private String email;
	private boolean premium;
	private ArrayList<AdressElement> adresy = new ArrayList<AdressElement>();

	public User() {

	}

	public User(String username, String password, String email, boolean premium) {

		this.username = username;
		this.password = password;
		this.email = email;
		this.premium = premium;
	}

	private void createBasicAdresses() {
		adresy.add(new AdressElement("Zameldowania"));
		adresy.add(new AdressElement("Korsepondencyjny"));
		adresy.add(new AdressElement("Pracy"));
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String paswword) {
		this.password = paswword;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isPremium() {
		return premium;
	}

	public void setPremium(boolean premium) {
		this.premium = premium;
	}

	public ArrayList<AdressElement> getAdressy() {
		return adresy;
	}

	public AdressElement getAdres(String adressName) {
		for (AdressElement adres : adresy) {
			if (adres.getAdressName().equals(adressName)) {
				return adres;
			}
		}
		return null;
	}

	public void addAdress(AdressElement newAdress) {
		adresy.add(newAdress);
	}

	public void removeAdress(String adressName) {
		for (int i = 0; i < adresy.size(); i++) {
			if (adresy.get(i).getAdressName().equals(adressName)) {
				adresy.remove(i);
				break;
			}
		}
	}
}

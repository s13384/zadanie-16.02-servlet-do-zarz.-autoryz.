package filters;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import models.User;
import nosqldb.NoSqlDataBase;

public class ShowProfileFilter implements Filter {

	FilterConfig filterConfig = null;
	private NoSqlDataBase database = NoSqlDataBase.getInstance();

	public void init(FilterConfig filterConfig) throws ServletException {
		this.filterConfig = filterConfig;
		database = NoSqlDataBase.getInstance();
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse resp = (HttpServletResponse) response;

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		boolean userInDb = false;

		User tmpUser = database.getUser((String) req.getAttribute("username"));

		if (tmpUser != null) {
			if (tmpUser.getPassword().equals(database.getUser((String) req.getAttribute("username")))) {
				userInDb = true;
				req.getSession().setAttribute("User", tmpUser);
			}
		}

		if (!userInDb) {
			// resp.sendRedirect("");
			out.print("Login lub haslo niepoprawne<br/>");
			out.println("<a href = 'login'>Sproboj Ponownie <a/>");
		} else {
			chain.doFilter(req, resp);

		}

	}

}
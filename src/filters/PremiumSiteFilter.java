package filters;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import models.User;
import nosqldb.NoSqlDataBase;

public class PremiumSiteFilter implements Filter {

	FilterConfig filterConfig = null;
	private NoSqlDataBase database = NoSqlDataBase.getInstance();

	public void init(FilterConfig filterConfig) throws ServletException {
		this.filterConfig = filterConfig;
		database = new NoSqlDataBase();
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse resp = (HttpServletResponse) response;

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		User tmpUser = (User) req.getSession().getAttribute("User");

		if (!tmpUser.isPremium()) {
			// resp.sendRedirect("");
			out.print("Niestery nie posiadasz konta premium<br/>");
			out.println("<a href = 'showProfile'>Powrot<a/>");
		} else {
			chain.doFilter(req, resp);

		}

	}

}
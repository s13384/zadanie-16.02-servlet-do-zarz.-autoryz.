package filters;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import models.User;
import nosqldb.NoSqlDataBase;

public class RegisterFilter implements Filter {

	FilterConfig filterConfig = null;
	private NoSqlDataBase database = NoSqlDataBase.getInstance();

	public void init(FilterConfig filterConfig) throws ServletException {
		this.filterConfig = filterConfig;
		database = NoSqlDataBase.getInstance();
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse resp = (HttpServletResponse) response;

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		if (newUserIsCorrect(req)) {
			addNewUserToDb(req);
			chain.doFilter(req, resp);
		} else {
			resp.sendRedirect("registrationFailed");
		}

	}

	private void addNewUserToDb(HttpServletRequest request) {

		database.addUser(new User(request.getParameter("username"), request.getParameter("password"),
				request.getParameter("email"), false));
	}

	private boolean newUserIsCorrect(HttpServletRequest request) {
		if (request.getParameter("username") == null)
			return false;
		if (request.getParameter("password") == null)
			return false;
		if (request.getParameter("confirm_password") == null)
			return false;
		if (request.getParameter("email") == null)
			return false;

		if (request.getParameter("username").toLowerCase().equals("admin"))
			return false;
		if (request.getParameter("password").equals(request.getParameter("confirm_password")))
			return true;

		return false;
	}

}
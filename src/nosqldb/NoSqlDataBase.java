package nosqldb;

import java.util.ArrayList;

import models.User;

public class NoSqlDataBase {

	private static NoSqlDataBase database;
	private ArrayList<User> users = new ArrayList<User>();

	private synchronized NoSqlDataBase NoSqlDataBase() {
		if (database == null)
			database = new NoSqlDataBase();
		createAdmin();
		return database;
	}

	public static NoSqlDataBase getInstance() {
		return database;
	}

	public ArrayList<User> getUsers() {
		return users;
	}

	public User getUser(String username) {
		for (int i = 0; i < users.size(); i++) {
			User user = users.get(i);
			if (user.getUsername().equals(username))
				return user;
		}
		return null;
	}

	public void addUser(User user) {
		users.add(user);
	}

	public boolean containsUser(String username) {
		for (int i = 0; i < users.size(); i++) {
			User user = users.get(i);
			if (user.getUsername().equals(username))
				return true;
		}
		return false;
	}

	private void createAdmin() {
		users.add(new User("admin", "password", "admin@servlets.pl", true));

	}
}

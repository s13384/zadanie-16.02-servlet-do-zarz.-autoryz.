package forms;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class RegisterForm extends javax.servlet.http.HttpServlet implements javax.servlet.Servlet {

	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		performTask(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		performTask(request, response);
	}

	private void performTask(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html");

		PrintWriter out = response.getWriter();
		out.println("<html><body><h2>Zarejestruj sie</h2>" + "<form action='printUser' method='post'>"
				+ "Username: <input type='text' name='username' /> <br />"
				+ "Password: <input type='password' name='password' /> <br />"
				+ "Confirm Password: <input type='password' name='confirm_password' /> <br />"
				+ "Email: <input type='text' name='email' /> <br />" 
				+ "<input type='submit' value='Register' />"
				+ "</form>" 
				+"<a href = 'showUsers'>Pokaz wszystkich uzytkownikow <a/>"
				+ "</body></html>");
		out.close();
	}

}